package com.example.recuperacionc1_java;

public class Cotizacion {

    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazo;

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getFolio() {
        return this.folio;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getValorAuto() {
        return this.valorAuto;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public float getPorEnganche() {
        return this.porEnganche;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public int getPlazo() {
        return this.plazo;
    }


    //Métodos de la aplicacion
    public int generarFolio() {
        for (int x=0;x>=99999999;x++){
            folio=folio+1;
        }
        return folio;
    }

    public float calcularEnganche() {
        float enganche;
        enganche=valorAuto*(porEnganche/100);
        return enganche;
    }

    public float calcularPagoMensual() {
        float mensualidad=0.0F;
        int plazo = 0;

        if (plazo == 1){
            mensualidad = valorAuto - calcularEnganche()/12;
        } else if (plazo == 2) {
            mensualidad = valorAuto - calcularEnganche()/18;
        } else if (plazo == 3) {
            mensualidad = valorAuto - calcularEnganche()/24;
        } else if (plazo == 4) {
            mensualidad = valorAuto - calcularEnganche()/36;
        }

        return mensualidad;
    }




}
