package com.example.recuperacionc1_java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtCliente;
    private Button btnCotizacion;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });
    }


    private void iniciarComponentes() {
        txtCliente = (EditText) findViewById(R.id.txtNombreCliente);
        btnCotizacion = (Button) findViewById(R.id.btnCotización);
    }

    private  void entrar(){
        String nombre = txtCliente.getText().toString().trim();

        if (nombre.isEmpty()) {
            Toast.makeText(this.getApplicationContext(), "Ingrese el nombre del cliente", Toast.LENGTH_SHORT).show();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtCliente.getText().toString());


            Intent intent = new Intent(MainActivity.this, CotizacionActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        }
    }



}